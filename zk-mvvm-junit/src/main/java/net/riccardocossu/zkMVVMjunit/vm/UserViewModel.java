package net.riccardocossu.zkMVVMjunit.vm;

import net.riccardocossu.zkMVVMjunit.dao.UserDAO;
import net.riccardocossu.zkMVVMjunit.model.User;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.Window;

public class UserViewModel {
	private User user;

	private UserDAO userDao = new UserDAO();

	@Init
	public void init(@ContextParam(ContextType.COMPONENT) Window w) {

	}

	@NotifyChange(value = { "user" })
	public void findById(@BindingParam("id") Long id) {
		this.user = userDao.findById(id);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setUserDao(UserDAO userDao) {
		this.userDao = userDao;
	}

}
