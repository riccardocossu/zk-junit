package net.riccardocossu.zkMVVMjunit.vm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import net.riccardocossu.zkMVVMjunit.dao.UserDAO;
import net.riccardocossu.zkMVVMjunit.model.User;

import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class UserViewModelMockitoTest {

	public static final String USER_NAME = "name";

	@Test
	public void usernameShouldChange() {
		// inizializzo il viewmodel come un oggetto qualsiasi
		UserViewModel vm = new UserViewModel();
		// sostituisco al volo l'implementazione del servizio che fornisce i
		// dati
		UserDAO dao = mock(UserDAO.class);
		vm.setUserDao(dao);
		when(dao.findById(anyLong())).thenAnswer(new Answer<User>() {
			public User answer(InvocationOnMock invocation) throws Throwable {
				User res = new User();
				res.setId((Long) invocation.getArguments()[0]);
				res.setUsername(USER_NAME);
				return res;
			}
		});
		// verifico le precondizioni
		assertNull(vm.getUser());
		// eseguo un'operazione che da contratto dovrebbe modificare il model
		vm.findById(1l);
		// verifico che il model è stato modificato
		assertNotNull(vm.getUser());
		assertEquals(USER_NAME, vm.getUser().getUsername());
	}

	@Test
	public void usernameDoesntChangeIfNoMock() {
		UserViewModel vm = new UserViewModel();
		assertNull(vm.getUser());
		vm.findById(1l);
		assertNull(vm.getUser());
	}
}
