package net.riccardocossu.zkMVVMjunit.vm;

import static org.junit.Assert.*
import net.riccardocossu.zkMVVMjunit.dao.UserDAO
import net.riccardocossu.zkMVVMjunit.model.User

import org.junit.Test

class UserViewModelTest {

	static final String USERNAME = 'name'

	@Test
	public void usernameShouldChange() {
		// inizializzo il viewmodel come un oggetto qualsiasi
		UserViewModel vm = new UserViewModel()
		// sostituisco al volo l'implementazione del servizio che fornisce i dati
		vm.userDao = [findById : { Long id ->
				return new User(id: id, username: USERNAME)
			}] as UserDAO
		// verifico le precondizioni
		assertNull(vm.user)
		// eseguo un'operazione che da contratto dovrebbe modificare il model
		vm.findById(1 as Long)
		// verifico che il model è stato modificato
		assertNotNull(vm.user)
		assertEquals(USERNAME,vm.user.username)
	}

	@Test
	public void usernameDoesntChangeIfNoMock() {
		UserViewModel vm = new UserViewModel()
		assertNull(vm.user)
		vm.findById(1 as Long)
		assertNull(vm.user)
	}
}
