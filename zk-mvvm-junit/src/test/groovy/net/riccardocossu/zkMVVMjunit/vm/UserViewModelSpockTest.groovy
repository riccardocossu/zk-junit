package net.riccardocossu.zkMVVMjunit.vm;

import static org.junit.Assert.*
import net.riccardocossu.zkMVVMjunit.dao.UserDAO
import net.riccardocossu.zkMVVMjunit.model.User
import spock.lang.Specification

class UserViewModelSpockTest extends Specification {
	static final String USERNAME = 'name'

	def "Username will change if mocked" () {
		setup:
		UserViewModel vm = new UserViewModel()
		vm.userDao = [findById : { Long id ->
				return new User(id: id, username: USERNAME)
			}] as UserDAO

		expect:
		vm.user == null

		when:
		vm.findById(1 as Long)

		then:
		vm.user != null
		vm.user.username.equals(USERNAME)
	}

	def "Username will not change if not mocked" () {
		setup:
		UserViewModel vm = new UserViewModel()

		expect:
		vm.user == null

		when:
		vm.findById(1 as Long)

		then:
		vm.user == null
	}
}
